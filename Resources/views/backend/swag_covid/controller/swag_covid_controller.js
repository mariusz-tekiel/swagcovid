// This is the controller


Ext.define('Shopware.apps.SwagCovid.controller.SwagCovidController', {
    /**
     * Override the swag_covid main controller
     * @string
     */
    override: 'Shopware.apps.Customer.controller.Main',

    init: function () {
        var me = this;

        // me.callParent will execute the init function of the overridden controller
        me.callParent(arguments);
    }
});
