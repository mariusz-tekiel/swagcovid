Ext.define('Shopware.apps.SwagCovid.model.Covid', {
    extend: 'Shopware.data.Model',

    configure: function() {
        return {
            controller: 'SwagCovid'
        };
    },

    fields: [
        { name : 'id', type: 'int', useNull: true },
        { name : 'headline', type: 'string' },
        { name : 'bg_colour', type: 'string' },
        { name : 'bg_image', type: 'string' },
        { name : 'main_text', type: 'string' },
        { name : 'more_info_button', type: 'string' },

    ]
});